/**
 * All jQuery functions in the system will be places in this file.
 *
 * Long description for class (if any)...  (should write)
 * @author     OpenArc Online <info@openarconline.com>
 * @copyright  1990-2013 OpenArc Systems Management(Pvt) Ltd.
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://openarconline.com
 * @since      available since Release 1.0.0
 */

$(document).ready(function() {

    /**
     * Ajax base add Members to Groups.
     *
     * @author name      Dharshana Jayamaha
     *         email     dharshana@openarc.lk
     * @param  groupId   contain group id which need to added
     * @param  memberID  member's Id
     * @param  baseUrl   System baseurl, assigned in the page header
     * @return false     so that form/href not getting submitted.
     */
    $('.addMemberToGroup').click(function(){
        groupId = $("#groupId").val();
        memberID = $(this).attr('id');
        //alert(memberID);
        //console.log(memberID);
        var dataStream = ({
            memberID : memberID,
            groupId  : groupId
        });
        //alert(baseUrl);
        $.ajax({
            method:'POST',
            url: baseUrl+'admin/group/ajAddMember',
            data: dataStream,
            success:function (response){
                if(response==1){
                    $("#"+memberID).closest('tr').remove();
                    $.ajax({
                        method:'POST',
                        data:dataStream,
                        url: baseUrl+'admin/group/ajloadmembers',
                        success:function(response){
                            //alert(response);
                            $("#aj_groupmembers").html(response);
                        }
                    });
                }
            }
        });
        return false;
    });
    $('#memberstable tr.dataRow').click(function(){
        //alert('hello');
        $(".rowSelect").find("i").remove();
        $(".rowSelect").removeClass("rowSelect");
        $(this).addClass("rowSelect");
        $(".rowSelect td.iconSelect").html('<i class="icon-ok"></i>');
        memberID = $(".rowSelect").attr('id');
        memberName = $(".rowSelect td.memberName").html();
        $("#group_admin2").val(memberName);
        $("#group_admin").val(memberID);
    });


    /*********events js****by Hasitha********/

    //this function runs when click on add event member button
    $("td a").filter(".eventAdd").click(function() {
        var eventId         = $("#heddenEventId").val();
        var member_value    = $(this).attr( 'id' );

        var datastreem = ( {
             member_detail : member_value,
                            event_details : eventId
                        });
             $.ajax({
                method:'POST',
                url: baseUrl+'admin/event/ajAddEventMember',
                data: datastreem,
                success:function (response){
                    if(response==1){
                        $("#"+member_value).closest('tr').remove();
                        $.ajax({
                            method:'POST',
                            data:datastreem,
                            url: baseUrl+'admin/event/ajloadEventMembers',
                            success:function(response){
                                //alert(response);
                                $("#aj_eventaddmember").html(response);
                            }
                        });
                    }
                }
            });
        return false;
            
    });

    //this function runs when click on add event group button
    $("td").find(".eventGroupAdd").each(function()
    {
        $(this).click(function()
        {
            var idName         = $(this).attr("id");
            var eventId        = $("#heddenEventId").val();
            var group_value    = $(this).attr( 'id' );

            var datastreem = ({
                                group_detail  : group_value, //group ID
                                event_details : eventId
                            });

            $.ajax(
            {
                method:'POST',
                url: baseUrl+'admin/event/ajAddeventGroup',
                data: datastreem,
                success:function (response)
                {
                    if(response == 1)
                        {
                            a = $("#groupsList").find('#'+idName).closest('tr').remove();
                            //alert(a);
                            $.ajax({
                                method:'POST',
                                data:datastreem,
                                url: baseUrl+'admin/event/ajloadEventGroups',
                                success:function(response){
                                    //alert(response);
                                    $("#aj_eventAddGroup").html(response);
                                    $.ajax({
                                        method:'POST',
                                        data:datastreem,
                                        url: baseUrl+'admin/event/ajloadEventMembers',
                                        success:function(response){
                                            //alert(response);
                                            $("#aj_eventaddmember").html(response);
                                        }
                                    });
                                }
                            });
                        }
                }
            });
            return false;
        });
    });


/***********end of events js***************/


});