/**
 * All jQuery functions in the system will be places in this file.
 *
 * Long description for class (if any)...  (should write)
 * @author     OpenArc Online <info@openarconline.com>
 * @copyright  1990-2013 OpenArc Systems Management(Pvt) Ltd.
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://openarconline.com
 * @since      available since Release 1.0.0
 */

$(document).ready(function() {

    /**
     * Ajax base add Members to Groups.
     *
     * @author name      Dharshana Jayamaha
     *         email     dharshana@openarc.lk
     * @param  groupId   contain group id which need to added
     * @param  memberID  member's Id
     * @param  baseUrl   System baseurl, assigned in the page header
     * @return false     so that form/href not getting submitted.
     */

     /*Code by Shashiprabhath to rpevent collision with modal closing*/
    $('.preview-pane-close').click(function(event)
    {
      event.preventDefault();
      $('.preview-pane').modal('hide');
    });
    /*EOF Code by Shashiprabhath to rpevent collision with modal closing*/

    /*Code by Shashiprabhath to send ajax data to back end for sending email/printing*/
    $("#recpient-print, #recpient-email").click(function(event)
    {
        event.preventDefault();

        //get clicked button's id to check if the request is for print or
        var mod = this.id;

        var checkedRecipients = $("#recipients input[type=checkbox]:checked").length;
        var msgSubject        = $("#tmp_email_subject").val();

        var contacts    = new Array();
        var accounts    = new Array();
        var con_groups  = new Array();
        var thm_cat     = new Array();

        //push checked contacts to array
        $("#contacts input[type=checkbox]").each(function(){
            if($(this).is(':checked'))
            {
                contacts.push($(this).val());
            }
        });

        //push checked accounts to array
        $("#accounts input[type=checkbox]").each(function(){
            if($(this).is(':checked'))
            {
                accounts.push($(this).val());
            }
        });

        //push checked contact groups to array
        $("#contact_groups input[type=checkbox]").each(function(){
            if($(this).is(':checked'))
            {
                con_groups.push($(this).val());
            }
        });

        //push checked thematic categories to array
        $("#thematic_categories input[type=checkbox]").each(function(){
            if($(this).is(':checked'))
            {
                thm_cat.push($(this).val());
            }
        });

        $("#contacts_hf").val(contacts);
        $("#accounts_hf").val(accounts);
        $("#con_groups_hf").val(con_groups);
        $("#thm_cat_hf").val(thm_cat);

        //if print button is pressesd
        if(mod=="recpient-print")
        {
            $("#delivery_type").val('P');
        }
        else
        {
            $("#delivery_type").val('E');
        }

        if(checkedRecipients==0)
        {
            $("#recipient-error").parent().css('display','');
            $("#recipient-error").html("Please Select at Least One Recipient");
        }
        else if(msgSubject=="")
        {
            $("#recipient-error").parent().css('display','');
            $("#recipient-error").html("Please Enter a Subject");
        }
        else
        {
            //submit the form
            document.template_submit.submit();
        }

        //get the content of template
        var templateContent   = $("#templateContent").html();

        var templateSubject   = $("#recpient-tmp_email_subject").val();

        //creates the final datastream
        var datastream      = ({accounts:accounts,
            contacts:contacts,
            con_groups:con_groups,
            thm_cat:thm_cat,
            tmpl_content:templateContent,
            subject:templateSubject
        });
        /*
         //send necessary data to the backend
         $.ajax({
         type:'POST',
         url:baseUrl+"admin/mail_wizard/pre_process",
         data:datastream,
         contentType : 'application/pdf',
         success:function(response)
         {

         }
         })
         */
    });
    // END OF send ajax data to back end for sending email/printing*/

    //Validation of email/pdf module by Shashiprabhath
    $("#templateSelectModelBtn").click(function(event)
    {
      var checkedRadioBtns = $("#myModalTemplate :radio[name='template']:checked").length;

      if(checkedRadioBtns==0)
      {
        event.preventDefault();
        $("#myModalTemplateError").html('Please select a template to proceed');
        $("#myModalTemplateError").css('display','');
      }
    });
    //EOF Validation of email/pdf module by Shashiprabhath



    $('.addMemberToGroup').click(function(){
        groupId = $("#groupId").val();
        memberID = $(this).attr('id');
        //alert(memberID);
        //console.log(memberID);
        var dataStream = ({
            memberID : memberID,
            groupId  : groupId
        });
        //alert(baseUrl);
        $.ajax({
            method:'POST',
            url: baseUrl+'admin/group/ajAddMember',
            data: dataStream,
            success:function (response){
                if(response==1){
                    $("#"+memberID).closest('tr').remove();
                    $.ajax({
                        method:'POST',
                        data:dataStream,
                        url: baseUrl+'admin/group/ajloadmembers',
                        success:function(response){
                            //alert(response);
                            $("#aj_groupmembers").html(response);
                        }
                    });
                }
            }
        });
        return false;
    });

    $('#memberstable tr.dataRow').click(function(){
        //alert('hello');
        $(".rowSelect").find("i").remove();
        $(".rowSelect").removeClass("rowSelect");
        $(this).addClass("rowSelect");
        $(".rowSelect td.iconSelect").html('<i class="icon-ok"></i>');
        memberID = $(".rowSelect").attr('id');
        memberName = $(".rowSelect td.memberName").html();
        $("#group_admin2").val(memberName);
        $("#group_admin").val(memberID);
    });

    /******add event organizer**********/

    $('#EventOrganizerstable tr.dataRow').click(function(){
        $(".rowSelect").find("i").remove();
        $(".rowSelect").removeClass("rowSelect");
        $(this).addClass("rowSelect");
        $(".rowSelect td.iconSelect").html('<i class="icon-ok"></i>');
        memberID = $(".rowSelect").attr('id');
        memberName = $(".rowSelect td.memberName").html();
        $("#organizer_id2").val(memberName);
        $("#organizer_id").val(memberID);
    });

    /**********end********************/


    /*********events js****by Hasitha********/

    //this function runs when click on add event member button
    $("td a").filter(".eventAdd").click(function() {
        var eventId         = $("#heddenEventId").val();
        var member_value    = $(this).attr( 'id' );

        var datastreem = ( {
             member_detail : member_value,
                            event_details : eventId
                        });
             $.ajax({
                method:'POST',
                url: baseUrl+'admin/event/ajAddEventMember',
                data: datastreem,
                success:function (response){
                    if(response==1){
                        $("#"+member_value).closest('tr').remove();
                        $.ajax({
                            method:'POST',
                            data:datastreem,
                            url: baseUrl+'admin/event/ajloadEventMembers',
                            success:function(response){
                                //alert(response);
                                $("#aj_eventaddmember").html(response);
                            }
                        });
                    }
                }
            });
        return false;
            
    });

    //this function runs when click on add event group button
    $("td").find(".eventGroupAdd").each(function()
    {
        $(this).click(function()
        {
            var idName         = $(this).attr("id");
            var eventId        = $("#heddenEventId").val();
            var group_value    = $(this).attr( 'id' );

            var datastreem = ({
                                group_detail  : group_value, //group ID
                                event_details : eventId
                            });

            $.ajax(
            {
                method:'POST',
                url: baseUrl+'admin/event/ajAddeventGroup',
                data: datastreem,
                success:function (response)
                {
                    if(response == 1)
                        {
                            a = $("#groupsList").find('#'+idName).closest('tr').remove();
                            //alert(a);
                            $.ajax({
                                method:'POST',
                                data:datastreem,
                                url: baseUrl+'admin/event/ajloadEventGroups',
                                success:function(response){
                                    //alert(response);
                                    $("#aj_eventAddGroup").html(response);
                                    $.ajax({
                                        method:'POST',
                                        data:datastreem,
                                        url: baseUrl+'admin/event/ajloadEventMembers',
                                        success:function(response){
                                            //alert(response);
                                            $("#aj_eventaddmember").html(response);

                                            $.ajax({
                                                method:'POST',
                                                data:datastreem,
                                                url: baseUrl+'admin/event/ajeventparticipationstatics',
                                                success:function(response){
                                                  //alert(response);
                                                  $("#aj_eventparticipationstatics").html(response);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                }
            });
            return false;
        });
    });


/***********end of events js***************/
/*************Adding members to Group ajax function 
*
***Coded By Priyanwada
**/
  $('td a').filter('.addMemberToGroupnot_in_group').click(function(event)
  {
    event.preventDefault();
    var MemberId = $("#MemberId").val();
    var groupID  = $(this).attr('id');

      var dataStream = ({
          MemberId : MemberId,
          groupID  : groupID
      });
        
      // alert(baseUrl);
        //console.log(memberID);
    $.ajax({
            method:'POST',
            url: baseUrl+'admin/member/ajAddGroup',
            data: dataStream,
            success:function (responce)
            {
              if (responce==1) 
              {
              $('#'+groupID).closest('tr').remove();
              $.ajax({
                      method:'POST',
                      data:dataStream,
                      url: baseUrl+'admin/member/ajloadGroup',
                      success:function(responce)
                      {
                        $("#aj_membersgroup").html(responce);
                      } 
                    });
              }
              else
              {
                alert('nooo');
              }
            }
    });
  });
/*********Ended of Adding members to Group ajax function *************/

/*********Adding members to Event ajax function*********/

$('td a').filter('.addMemberToEventnot_in_event').click(function(event)
{
    event.preventDefault();
    var MemberId = $("#MemberId").val();
    var eventID  = $(this).attr('id');
    var idnm   = $(this).attr("id");
    
     var dataStreams = ({
          MemberId : MemberId,
          eventID  : eventID
      });
     	$.ajax({
       		method:'POST',
            url: baseUrl+'admin/member/ajAddEvent',
            data: dataStreams,
            success:function (response)
            {
             if (response==1) 
              {
              	$("#Eventlist").find('#'+idnm).closest('tr').remove();	
              //$('#'+eventID).closest('tr').remove();
              $.ajax({
                      method:'POST',
                      data:dataStreams,
                      url: baseUrl+'admin/member/ajloadEvent',
                      success:function(response)
                      {
                        $("#aj_membersevent").html(response);
                      } 
                    });
              }
              else
              {
                alert('nooo');
              }
            }
    });
      
      
    
      
    
});
/*********Ended of Adding members to Event ajax function*********/

// CRM

//setting email
$('.email').click(function(){
   var email = $(this).html();
   // alert(email);
    $('#email_to').val(email);
});

//    $('#sendEmail').click(function(){
//
//        var to = $('#email_to').val();
//        var from =  $('#fromEmail').val();
//        var subject = $('#email_subject').val();
//        var message = $('.mceContentBody').html();
//        alert(message);
//    });

    $('.thematic_cat_chkbox').click(function(){
        if ($(this).is (':checked')){
            //alert($(this).attr("catname"));
            $('#nothematic').html('');
            $('#selected_thematic_categories').append('<span class="label label-info temeticCat">'+$(this).attr("catname")+' <button style="float: none;" type="button" data-dismiss="alert" class="close remove_themetaic" chkid="'+$(this).attr("catid")+'">×</button></span> ');
            currentval = $('#contact_thematic_cagegories').val();
//            if(currentval==''){
//                $('#contact_thematic_cagegories').val($(this).attr('catid')+" ");
//            }else{
                $('#contact_thematic_cagegories').val(currentval+','+$(this).attr('catid')+" ");
//            }

        }else{
            $('#selected_thematic_categories').find('button[chkid='+$(this).attr("catid")+']').parent().remove();
            setvalue = $('#contact_thematic_cagegories').val();
            currentval = ","+$(this).attr("catid")+" ";
            $('#contact_thematic_cagegories').val(setvalue.replace(currentval,''));
        }
    });

    $(document).on('click', '.remove_themetaic', function() {
        chkid = $(this).attr("chkid");
        $('#thematic_cat_'+chkid).attr('checked', false);
        currentval = ","+chkid+" ";
        // alert(chkid);
        setvalue = $('#contact_thematic_cagegories').val();
        $('#contact_thematic_cagegories').val(setvalue.replace(currentval,''));
    });
});
