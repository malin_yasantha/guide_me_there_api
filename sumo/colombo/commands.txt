netconvert --node-files=hello.nod.xml --edge-files=hello.edg.xml --output-file=hello.net.xml
sumo-gui -c hello.sumocfg

--sl
netconvert --osm-files sl.osm -o hello.net.xml
polyconvert --net-file hello.net.xml --osm-files sl.osm --type-file typemap.xml -o sl.poly.xml
set path=C:\Python27
python randomTrips.py -n hello.net.xml -r sl.rou.xml -e 5000 -l

