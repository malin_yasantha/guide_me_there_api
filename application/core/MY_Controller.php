<?php
/**
 * -----------------------------IMPORTANT-------------------------------
 * Programmer should NOT change or add any code without having a better
 * understanding how MY_CONTROLLER and Its methods been used
 * ---------------------------------------------------------------------
 *
 * My_Controller will be used for all the CRUD operations in the system.
 *
 * All the other controllers should be extend form My_Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class MY_Controller extends CI_Controller
{
    protected $_error_code = 1;
    function __construct(){
        parent::__construct();
        $this->data['errors'] = array();
        $this->data['site_name'] = config_item('site_name');
    }

   
    /**
     * this function will be called to send email
     *
     * @param  String   $to         Receiver's email address
     * @param  String   $subject    Email Subject
     * @param  String   $message    Email Message
     * @param  String   $fromEmail  Sender's Email address, default set to 'noreply@guidemethere.lk'
     * @param  String   $fromName   Senders's Name, default set to 'Guidemethere'
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return TRUE if message send and FALSE if Failed.
     */
    protected function sendEmail($to, $subject, $message, $fromEmail = 'noreply@guidemethere.lk' , $fromName = "Guidemethere")
    {

        $this->load->library('email');

        $this->email->set_newline("\r\n");
        $this->email->from($fromEmail, $fromName);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * this function will be called to send MASS emails
     *
     * @param  Array()   $to          Array receivers' Email address
     * @param  String    $subject     Email Subject
     * @param  String    $message     Email Message
     * @param  String    $fromEmail   Sender's Email Address, default set to 'noreply@guidemethere.lk'
     * @param  String    $fromName    Sender's Name, default set to 'Guidemethere'
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return NO returns
     */
    protected function sendMassEmail($to, $subject, $message, $fromEmail = "noreply@guidemethere.lk", $fromName = "Guidemethere")
    {
        foreach($to as $recipient)
        {
            if($this->sendEmail($recipient, $subject, $message, $fromEmail, $fromName)){
                //echo 'Email Send';
            }else{
                //echo 'NOT Send';
            }
        }

    }

    /**
     * this function return and download data from database by query
     *
     * @param  Array()   $queryData   codeigniter query return
     * @param  Bool      $headers     data table headlines will be table flied names
     * @param  String    $filename    download file name
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return NO returns
     */
    protected function toCSV($queryData, $headers = TRUE, $filename = 'data.csv'){
        query_to_csv($queryData, $headers, $filename);
    }

    /**
     * this function saves new sorting  order
     *
     * @param  json   JsonObject   Ajax pass
     * @param  String    $filename    download file name
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return NO returns
     */
    protected function save_order($oderjson= NULL)
    {

        $new_oder_json = $this->input->post('oderjson');
        $modal_name =  $this->input->post('modal').'_m';
        echo $modal_name;

        if ($new_oder_json == NULL) {

        } else {

            echo $new_oder_json;

            $new_oder = json_decode($new_oder_json);

            foreach ($new_oder as $id => $data) {
                $this->$modal_name->save(array(
                    'order_column'=>$data),$id);
            }

        } // if ($new_oder_json == NULL) {

    }
}
