<?php
/**
 * Test Model
 *
 * All database related operation related to tbl_users should be listed within this model
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */

class Junction_priority_m  extends MY_Model
{
    protected $_table_name = 'tbl_junction_priority';
    // protected $_order_by = 'id';
    protected $_timestamps = TRUE;

    function __construct(){
        parent::__construct();
    }

    public function saveData($data){
        $array = array();
        foreach($data as $key=>$value){
            $this->save(array('junction'=> $key, 'priority_count'=> $value));
        }
        
    }

}
