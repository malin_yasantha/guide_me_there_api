<?php

class Traffic_keywords_m  extends MY_Model{
    
    protected $_table_name = 'tbl_traffic_keywords';
    
    protected $_timestamps = FALSE;

    function __construct(){
        parent::__construct();
    }

    public function getData(){
        return $this->get();
        
    }

    public function get_estimated_late_time($key){
        
        return $this-> get_by(array('keywords'=>$key), TRUE);
        
    }

   
}