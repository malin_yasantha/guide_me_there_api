<?php

class Latest_news_date_m  extends MY_Model{
    
    protected $_table_name = 'tbl_latest_news';
    
    function __construct(){
        parent::__construct();
    }

    public function save_latest_news_date($data){
        $this->hardDelete_all();
        return $this->save($data);
    }

    
   
}