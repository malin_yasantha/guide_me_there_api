<?php
class Ctrl_rss_analyzer extends My_Controller
{
    //protected $_error_code = 10;
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Test_m');
        $this->load->helper('string');
        $this->load->helper('xml');
        $this->load->model('Traffic_keywords_m');
        $this->load->model('Latest_news_date_m');
        $this->load->model('Get_latest_news_date_m'); 
        $this->load->model('Final_news_details_m');
        $this->load->library('sentimentanalyzer');
        ini_set('max_execution_time', 300);
    }   
    
        
    //get rss feed data
    public function getrss_feed()
    {
             
        //check rssfeed xml exsist or not
        if (file_exists('files/Rssfeed.xml')) 
        {
        	$doc = new DOMDocument();
            $doc->load( 'files/Rssfeed.xml' );//xml file loading here

            $items = $doc->getElementsByTagName( "item" );
            
            $i = 1;//news count
            $result = array();
            $final_result = array();
            $single_record_array =array();
            $sentiment=array();
            $count=0;
            $sentiment_result=array();
            $final_sentiment_result=array();
            $sentiment_count=0;

            $db_result=array();
            $db_single_record=array();
            $final_db_result=array();
            
            //get latest news date
             $latest_date_db = $this->Get_latest_news_date_m->getData();   
             $final_latest_date_db=$latest_date_db[0]->latest_news_date;    

            foreach( $items as $item )
            {
              $keywords = array();
              
              //get news date and find the latest news
              $pubDates = $item->getElementsByTagName( "pubDate" );
              $pubDate =$pubDates->item(0)->nodeValue;
              $ppubDate = explode(" ",$pubDate);
              $time = strtotime($ppubDate[1] . '-' . $ppubDate[2] . '-' . $ppubDate[3] . ' ' .  $ppubDate[4]);
              $mysqldate = date('Y-m-d H:i:s', $time);//get news data ends
              
                      
                //check 
                if($count==0)
                {
                    $latest_news_date=$mysqldate;
                }
                $count++;
                
                if($mysqldate > $final_latest_date_db)
                {

                    $descriptions = $item->getElementsByTagName( "description" );
                    $description = $descriptions->item(0)->nodeValue;

                    //get keywords from database
                    $datas = $this->Traffic_keywords_m->getData();
                    
                    
                    foreach( $datas as $data )
                    {   
                        
                        $haystack = $description;
                        $needle   = $data->keywords;
                        
                        if( strpos( $haystack, $needle ) !== false ) 
                        {
                            //get estimated late time from db
                            $relavant_key=$needle;
                            $rowdata=$this->Traffic_keywords_m->get_estimated_late_time($relavant_key);
                            $latetime=$rowdata->estimated_time;
                            //get junctions from map.net.xml for process
                            if (file_exists('sumo/map.net.xml')) 
                            {
                                $doc = new DOMDocument();
                                $doc->load( 'sumo/map.net.xml' );//xml file loading here

                                $nets  = $doc->getElementsByTagName("net");
                                
                                foreach( $nets as $net )
                                {
                                    $junctions = $net->getElementsByTagName( "junction" );
                                    foreach( $junctions as $junction)
                                    {
                                        $type=$junction->getAttribute('type');
                                                
                                        if($type=="priority" || $type=="dead_end")
                                        {
                                            $node=$junction->getAttribute('id');
                                            //check junction found or not
                                            if( strpos( $haystack, $node ) !== false ) 
                                            {
                                                array_push($keywords, $node);
                                                
                                                
                                            }
                                                    
                                        }

                                    }
                                } 
                            }
                            else
                            {
                            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'File not found');
                            //echo json_encode($error);
                            }//get junctions from map.net.xml for process ends here

                            if(ENVIRONMENT == 'development'){
                                //sentiment analyzing function call
                                $this->sentimentanalyzer->insertTestData('http://localhost/guide_me_there_api/sentimental_data/trainingSet/data.neg', 'negative',5000);    
                                $this->sentimentanalyzer->insertTestData('http://localhost/guide_me_there_api/sentimental_data/trainingSet/data.pos', 'positive',5000); 
                            } else if(ENVIRONMENT == 'production'){
                                //sentiment analyzing function call
                                $this->sentimentanalyzer->insertTestData('http://54.169.39.187/sentimental_data/trainingSet/data.neg', 'negative',5000);    
                                $this->sentimentanalyzer->insertTestData('http://54.169.39.187/sentimental_data/trainingSet/data.pos', 'positive',5000); 
                            }
                                      
                            $sentiment_details=$this->sentimentanalyzer->analyzeSentence($haystack);
                            
                            $sentiment_result=array_merge($sentiment_result,$sentiment_details);
                            $single_sentiment_record_array=array('sentiment_single_record'=>$sentiment_result);
                            array_push($final_sentiment_result,$single_sentiment_record_array);
                            $sentiment_count++;//sentiment ends here
                            

                            //Only for a traffic related news process goes here
                            $new_array = array('affected_nodes'=> $keywords, 'Estimated_late_time'=> $latetime, 'reason'=>$relavant_key,'sentiment_details'=>$final_sentiment_result);
                            $result = array_merge($result, $new_array);
                            $single_record_array=array('single_record'=>$result);
                            $i++;
                            array_push($final_result,$single_record_array);
                            //traffic related process ends here
                            

                        }
                    }
                }
            }
         
        //insert into latest_news table
        $data=array('latest_news_date'=>$latest_news_date);
        $this->Latest_news_date_m->save_latest_news_date($data);//insertion end


            //insertion into news table
            for($j=0;$j<sizeof($final_result);$j++)
            {
                 
                 $affected_nodes=$final_result[$j]['single_record']['affected_nodes'];
                 $Estimated_late_time=$final_result[$j]['single_record']['Estimated_late_time'];
                 $reason=$final_result[$j]['single_record']['reason'];
                 $sentiment=$final_result[$j]['single_record']['sentiment_details'][$j]['sentiment_single_record']['sentiment'];
                 $positivity=$final_result[$j]['single_record']['sentiment_details'][$j]['sentiment_single_record']['accuracy']['positivity'];
                 
                 $negativity=$final_result[$j]['single_record']['sentiment_details'][$j]['sentiment_single_record']['accuracy']['negativity'];
                 $str_affNodes = '';
                 
                 foreach($affected_nodes as $affected_node){
                    $str_affNodes = $str_affNodes . ' ' .$affected_node;
                 }
                 //insertion
                 $arrayFinal_final=array('affected_nodes'=>$str_affNodes, 'Estimated_late_time'=>$Estimated_late_time, 'reason'=>$reason,'sentiment'=>$sentiment,'positivity'=>$positivity,'negativity'=>$negativity);

                //  //calling model
                 $this->Final_news_details_m->save_news($arrayFinal_final);
            }//insertion ends here

            //final json object displaying part goes here
            //take details from db
            $final_datas = $this->Final_news_details_m->get_news_Data();
            foreach( $final_datas as $data )
            {   
                        
                        $db_affected_nodes   = $data->affected_nodes;
                        $db_Estimated_late_time   = $data->Estimated_late_time;
                        $db_reason   = $data->reason;
                        $db_sentiment   = $data->sentiment;
                        $db_positivity   = $data->positivity;
                        $db_negativity=$data->negativity;

                        $final_displayed_data=array('affected_nodes'=>$db_affected_nodes, 'Estimated_late_time'=>$db_Estimated_late_time, 'reason'=>$db_reason ,'sentiment'=>$db_sentiment,'positivity'=>$db_positivity,'negativity'=>$db_negativity);
                        $db_result = array_merge($db_result,$final_displayed_data);
                        $db_single_record=array('single_record'=>$db_result);
                        array_push($final_db_result,$db_single_record);
                        
            }//displaying news ends here
            echo json_encode($final_db_result);

            
       
      }
      else{
        $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'File not found');
        echo json_encode($error);
      }
    }
}
