<?php
/**
 * Path Analyzer Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class Ctrl_chat_bot extends My_Controller
{
    protected $_error_code = 400;
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('string');
        $this->load->helper('xml');
        ini_set('max_execution_time', 300);
    }   

    public function index(){
    }
    
    //Ctrl_Path_generator output should come here
    public function test(){

    }
}
