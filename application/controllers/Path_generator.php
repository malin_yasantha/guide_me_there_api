<?php
/**
 * Path Generator Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class Path_generator extends My_Controller
{
    //protected $_error_code = 10;
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('string');
        $this->load->helper('xml');
    }   

    function test(){

      $graph = [
        ['A', 'B'],
        ['A', 'D'],
        ['A', 'E'],
        ['B', 'E'],
        ['B', 'C'],
        ['D', 'C'],
        ['D', 'K'],
        ['E', 'F'],
        ['E', 'B'],
        ['F', 'G'],
        ['G', 'K'],
        ['C', 'K'],
    ];

      $paths = $this->findPaths($graph, 'A', 'K');
      if (is_array($paths)) {
          $lines = $this->flatArray($paths);
          usort($lines, function($a, $b) {
              $al = strlen($a);
              $bl = strlen($b);
              if ($al == $bl) { return 0; }
              return ($al < $bl) ? -1 : 1;
          });
          foreach ($lines as $line) {
              echo $line,"\n";
          }
      } else {
          echo 'Can\'t find valid path.';
      }
    }
    
  function findPaths(array $graph, $from, $to) {
        $startPaths = $graph;
        // Search all possible start points
        // and removed them from possible next path
        foreach ($startPaths as $key => $path) {
            if ($path[0] != $from) {
                unset($startPaths[$key]);
            } else {
                unset($graph[$key]);
            }
        }
        // No start point found
        if (count($startPaths) == 0) {
            return false;
        }
        // Test all possible start points
        $stackPath = [];
        foreach($startPaths as $path) {
            if ($path[1] == $to) {
                return $path[0].$path[1];
            }
            if (!$found = $this->findPaths($graph, $path[1], $to)) {
                continue;
            }
            $stackPath[$path[0].$path[1]] = $found;
        }
        return $stackPath;
    }

  function flatArray($paths, $pre = '', $lines = []) {
      foreach($paths as $key => $path) {
          if (is_array($path)) {
              $lines = $this->flatArray($path, $pre.$key.' | ', $lines);
          } else {
              $lines[] = $pre.$key.' | '.$path;
          }
      }
      return $lines;
  }

}
