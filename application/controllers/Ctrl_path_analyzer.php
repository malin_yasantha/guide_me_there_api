<?php
/**
 * Path Analyzer Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class Ctrl_path_analyzer extends My_Controller
{
    protected $_error_code = 400;
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Test_m');
        $this->load->helper('string');
        $this->load->helper('xml');
        $this->load->library('regression');
        ini_set('max_execution_time', 300);
    }   

    public function index(){
        $this->regression->setXY(array(1 => 2, 2 => 1, 3 => 3));
        var_dump($this->regression->compluteY(4));
    }
    
    public function getPaths(){
      //getting all paths to an array
      $routesArray = array();
      if (file_exists('sumo/bus.trips.xml')) {
          $doc = new DOMDocument();
          $doc->load( 'sumo/bus.trips.xml' );
          $routes = $doc->getElementsByTagName( "routes" );
          foreach( $routes as $routes ) {
            $vehicles = $routes->getElementsByTagName( "vehicle" );
            foreach( $vehicles as $vehicle ){
              $routes = $vehicle->getElementsByTagName( "route" ); 
              $junctions = $routes->item(0)->getAttribute('junctions');
              $edges = $routes->item(0)->getAttribute('edges');
              $id = $vehicle->getAttribute('id');
              $a1 = array($id => array($junctions, $edges));
              //$a1 = array($id => array($junctions, ));
              $routesArray = $routesArray + $a1;
            }
          }
      }
      //var_dump($routesArray);
      return $routesArray;
    }

    public function generatePaths($start, $end){
      // $start = 'gneJ18';
      // $end = 'gneJ6';
      $finalRes = [];
      $paths = $this->getPaths();
      $i = 0;
      foreach ($paths as $bus_id => $objArray) {
        $str_junctions = $objArray[0];
        $str_edges = $objArray[1];
        $junctions = explode(" ", $str_junctions);
        $edges = explode(" ", $str_edges);
        if(in_array($start, $junctions)){
          $start_key = array_search($start, $junctions);
          $reponse = $this->findEnd($end, $junctions, $start_key, $edges);
          if($reponse){
            //on first iteration its working
            // $finalRes = $finalRes + array('bus_number' => $bus_id, 'junctions' => $reponse);
            //$array1 = array('bus_number' => $bus_id, 'junctions' => $reponse);
            $std2 = new stdClass;
            $std2->bus_number = $bus_id;
            $std2->junctions = $reponse[0];
            $std2->edges = $reponse[1];
            $tmp[] = $std2 ;
            $finalRes[] = $tmp;
            //array_merge($finalRes, 'route' => $array1);
            // var_dump(array('bus_number' => $bus_id, 'junctions' => $reponse));
            break;
          } else {
            $p_bus_id = $bus_id;
            $s_junctions = array_slice($junctions, $start_key);
            foreach ($s_junctions as $s_junction) {
              // 
              foreach ($paths as $bus_id => $objArray) {
                // var_dump($objArray);
                // die();
                $str_junctions = $objArray[0];
                $str_edges = $objArray[1];
                $junctions = explode(" ", $str_junctions);
                if(in_array($s_junction ,$junctions) && in_array($end, $junctions)){
                  $tmp = [];
                  //start of getting index to ignore past
                  $temp_s = array_search($s_junction, $junctions);
                  $temp_e = array_search($end, $junctions);
                  if($temp_s > $temp_e){
                    continue;
                  }
                  //end of

                  //getting first bus junction details
                  $end_key = array_search($s_junction, $s_junctions);
                  $end_key++;
                  $p_junctions = array_slice($s_junctions, 0, $end_key);
                  $tem_edges = array_slice($edges, 0, $end_key-1);
                  // $finalRes = $finalRes + array('bus_number' => $p_bus_id, 'junctions' => $p_junctions);
                  // var_dump(array('bus_number' => $p_bus_id, 'junctions' => $p_junctions));
                  // echo '<br>';
                  // array_push($temRes, array('bus_number' => $p_bus_id, 'junctions' => $p_junctions));
                  $std2 = new stdClass;
                  $std2->bus_number = $p_bus_id;
                  $std2->junctions = $p_junctions;
                  $std2->edges = $tem_edges;
                  $tmp[] = $std2 ;
                  
                  //getting second bus junction details
                  $start_key = array_search($s_junction, $junctions);
                  // var_dump($start_key);
                  // die();
                  $junctions = $this->findEnd($end, $junctions, $start_key, $edges);
                  // array_push($temRes, array('bus_number' => $bus_id, 'junctions' => $junctions));
                  $std2 = new stdClass;
                  $std2->bus_number = $bus_id;
                  $std2->junctions = $junctions[0];
                  $std2->edges = $junctions[1];
                  $tmp[] = $std2 ;
                  $finalRes[] = $tmp;
                  // var_dump(array('bus_number' => $bus_id, 'junctions' => $junctions));
                  // echo '<br><br>';
                  //$finalRes = $finalRes + array('bus_number' => $bus_id, 'junctions' => $junctions);
                  // array_merge($finalRes, array('bus_number' => $bus_id, 'junctions' => $junctions));
                }
              }
              // array_merge($finalRes, 'route' => $temRes);
              
            }
          }
        }
        $i++;
      }
      return $finalRes;
    }

    public function findEnd($end, $junctionsArray, $start_key, $edges){
      if(in_array($end, $junctionsArray)){
          $end_key = array_search ($end, $junctionsArray);
          $end_key = ($end_key - $start_key) + 1;
          $outputE = array_slice($edges, $start_key, $end_key - 1);
          $outputJ = array_slice($junctionsArray, $start_key, $end_key);
          return array($outputJ, $outputE);
      }
    }

    //Ctrl_Path_generator output should come here
    public function test(){
        $std = new stdClass;
        $std->bus_number = 'B1';
        $std->edges = 'gneE38 gneE25';
        $std->junctions = 'gneJ18 gneJ15 gneJ8';

        $std2 = new stdClass;
        $std2->bus_number = 'B2';
        $std2->edges = 'gneE14 gneE17 gneE2';
        $std2->junctions = 'gneJ8 gneJ9 gneJ3 hneJ2';


        $data = [];
        $data[0] = $std;
        $data[1] = $std2;


        $std3 = new stdClass;
        $std3->bus_number = 'B3';
        $std3->edges = 'gneE41 gneE51';
        $std3->junctions = 'gneJ18 gneJ28 gneJ11';

        $std4 = new stdClass;
        $std4->bus_number = 'B4';
        $std4->edges = 'gneE20 gneE17 gneE2';
        $std4->junctions = 'gneJ11 gneJ9 gneJ3 gneJ2';


        $data2 = [];
        $data2[0] = $std3;
        $data2[1] = $std4;

        $result = array(0 => $data2, 1 => $data);
        // var_dump($result);
        return $result;

    }

    public function analyzeData($start, $end){
        $timestep = '600.00';
        // $routes = $this->test();
        // $start = 'gneJ18';
        // $end = 'gneJ6';
        $routes = $this->generatePaths($start, $end);
        $best_index = 0; 
        $i = 0;
        $timestampArray = array();
        $a_timestampArray = array();
        foreach($routes as $route){
            //single route
            $min_time = 1000000000000000000;
            $current_route_index = $i;
            $i++;
            $tt = 0;
            foreach($route as $object){
                $edges = $object->edges;
                $junctions = $object->junctions;
                $currentEdges = $this->getCurrentEdges($timestep, $object->bus_number);
                if(!empty($currentEdges)){
                    //Assuming that one bus on one time
                    $start_edge = $currentEdges[0];  //Remove later
                    $end_edge = end($edges); 
                    // var_dump($object);
                    // echo '<br>';
                    $tt += $this->calculateTravelTime($object->bus_number, $timestep, $start_edge, $end_edge);
                    //array_push($timestampArray, $current_route_index => $tt);
                } else {
                    //skip this iteration since no busses avilable
                }
                //echo 'innerloop' . $tt . '<br>';
            }
            if($tt < $min_time){
                $min_time = $tt;
                $best_index = $current_route_index;
            }
            // echo 'outerloop' . $tt . '<br>';
            $new_array = array($current_route_index => $tt);
            $a_timestampArray = array_merge($timestampArray, $new_array);

            array_push($timestampArray, $tt);
        }
        $s_timestampArray = $this->quickSort($timestampArray);
        $final = array();
        $keyArray = array();
        foreach($s_timestampArray as $s_timestep){
            $key = array_search($s_timestep, $a_timestampArray);

            // if(in_array($key,$keyArray)){
            //   break;
            // }
            if(empty($routes[$key][0]->edges)){
              unset($routes[$key][0]);
              $routes[$key] = array_values($routes[$key]);
            }
            array_push($keyArray, $key);
            array_push($final, array('route' => $routes[$key]));
        }
        // $json_final = array();
        // foreach($final as $routes){
        //     $_final_busses = array();
        //     foreach ($routes as $busses) {
        //         $new_array = array('busses' => $busses);
        //         array_push($_final_busses, $new_array);
        //     }
        //     $new_array = array('routes' => $_final_busses);
        //     array_push($json_final, $new_array);
        // }
        echo json_encode($final);

    }

    public function quickSortTest(){
        $array = array(6,2,4,1);
        $array = $this->quickSort($array);
        print_r($array);
    }

    public function quickSort($array){
        if(count($array) < 2){
            return $array;
        }
        $left = $right = array();
        reset($array);
        $pivot_key  = key($array);
        $pivot  = array_shift($array);
        foreach($array as $k => $v) {
            if($v < $pivot)
                $left[$k] = $v;
            else
                $right[$k] = $v;
        }
        return array_merge($this->quickSort($left), array($pivot_key => $pivot), $this->quickSort($right));
    }

    public function calculateTravelTime($bus_id, $timestep, $start_edge, $end_edge){
        $edges = $this->getEdges($bus_id);
        $start_key = array_search ($start_edge, $edges);
        $end_key = array_search ($end_edge, $edges);
        $outputs = array_slice($edges, $start_key, $end_key+1);
        foreach($outputs as $output){
            $edgeLength = $this->getLengthOfEdge($output);
            $avgSpeed = $this->getAvgSpeedOfEdge($output, $timestep);
            return $edgeLength/$avgSpeed;

        }

    }

    public function getLengthOfEdge($edge_id){
        if (file_exists('sumo/map.net.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here
            
            $edges = $doc->getElementsByTagName( "edge" );
            foreach( $edges as $edge )
            {
                
                if($edge->getAttribute('id') == $edge_id){
                    $lane = $edge->getElementsByTagName( "lane" );
                    $length = $lane->item(0)->getAttribute('length');
                    return floatval($length);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

    public function getAvgSpeedOfEdge($edge_id, $p_timestep){
        if (file_exists('sumo/output.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/output.xml' );//xml file loading here
            $result = array();
            $i = 0;
            $timesteps = $doc->getElementsByTagName( "timestep" );
            foreach( $timesteps as $timestep )
            {
                if($timestep->getAttribute('time') == $p_timestep){
                    $edges = $timestep->getElementsByTagName( "edge" );

                    foreach( $edges as $edge ){
                        if($edge->getAttribute('id') == $edge_id){
                            $lanes = $edge->getElementsByTagName( "lane" );
                            $avgSpeed = 0;
                            $i = 0;
                            foreach($lanes as $lane){
                                $vehicles = $lane->getElementsByTagName( "vehicle" );
                                foreach($vehicles as $vehicle){
                                    $avgSpeed += floatval($vehicle->getAttribute('speed'));
                                    $i++;
                                }
                            }
                            if($i > 1){
                                return ($avgSpeed/$i);
                            }
                            else{ 
                                return $this->getSpeedOfEdge($edge_id);
                            }
                        }
                    }
                    return $this->getSpeedOfEdge($edge_id);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

    public function getSpeedOfEdge($edge_id){
        if (file_exists('sumo/map.net.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here
            
            $edges = $doc->getElementsByTagName( "edge" );
            foreach( $edges as $edge )
            {
                
                if($edge->getAttribute('id') == $edge_id){
                    $lane = $edge->getElementsByTagName( "lane" );
                    $speed = $lane->item(0)->getAttribute('speed');
                    return floatval($speed);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }


    public function getEdges($bus_id){
        if (file_exists('sumo/bus.trips.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/bus.trips.xml' );//xml file loading here

            $vehicles = $doc->getElementsByTagName( "vehicle" );
            foreach( $vehicles as $vehicle )
            {
                $r_bus_id = $vehicle->getAttribute('id');
                if($r_bus_id == $bus_id){
                    $route = $vehicle->getElementsByTagName( "route" );
                    $edges = $route->item(0)->getAttribute('edges');
                    $edgesArray = explode(" ",$edges);
                    return $edgesArray;
                }
            }
            //bus not found
            return -1;
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

    public function getCurrentEdges($p_timestep, $bus_id){
        if (file_exists('sumo/output.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/output.xml' );//xml file loading here
            $result = array();
            $i = 0;
            $timesteps = $doc->getElementsByTagName( "timestep" );
            foreach( $timesteps as $timestep )
            {
                $r_timestep = $timestep->getAttribute('time');

                if($r_timestep == $p_timestep){

                    $edges = $timestep->getElementsByTagName( "edge" );

                    foreach( $edges as $edge ){
                        $c_edge_id = $edge->getAttribute('id');
                        $lanes = $edge->getElementsByTagName( "lane" );
                        foreach($lanes as $lane){
                            $vehicles = $lane->getElementsByTagName( "vehicle" );
                            foreach ($vehicles as $vehicle) {
                                $vehicle_id = $vehicle->getAttribute('id');
                                if($vehicle_id[0] == "B"){
                                    if(substr($vehicle_id,0,2) == $bus_id){
                                        $result[$i] = $c_edge_id;
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                    return $result;
                }
            }
            //time step not found
            return NULL;
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

    public function trafficCountOnEdge($p_timestep, $edge_id){
        if (file_exists('sumo/output.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/output.xml' );//xml file loading here
            $timesteps = $doc->getElementsByTagName( "timestep" );
            foreach( $timesteps as $timestep )
            {
                $r_timestep = $timestep->getAttribute('time');

                if($r_timestep == $p_timestep){

                    $edges = $timestep->getElementsByTagName( "edge" );

                    foreach( $edges as $edge ){
                        $c_edge_id = $edge->getAttribute('id');
                        $i = 0;
                        if($c_edge_id == $edge_id){
                            $lanes = $edge->getElementsByTagName( "lane" );
                            foreach($lanes as $lane){
                                $vehicles = $lane->getElementsByTagName( "vehicle" );
                                foreach ($vehicles as $vehicle) {
                                    $speed = $vehicle->getAttribute('speed');
                                    if($speed == 0){
                                        $i++; 
                                    }
                                }
                            }
                            return $i;
                        }
                    }
                    //edge not found
                    return -1;
                }
            }
            //time step not found
            return -2;
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

    public function processOutputFile(){
        if (file_exists('sumo/output.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/output.xml' );//xml file loading here
            $timesteps = $doc->getElementsByTagName( "timestep" );

            

            foreach( $timesteps as $timestep )
            {
                $xml = new DOMDocument();
                $xml_data = $timestep;
                $time = $xml_data->getAttribute('time');
                $xml->appendChild( $xml->importNode( $xml_data, true ));
                $xml->save("sumo/outputs/$time.xml");
            }
			$error = array('status' => 'success', 'msg' => 'Files saved successfully');
            echo json_encode($error);
            die();
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }
}
