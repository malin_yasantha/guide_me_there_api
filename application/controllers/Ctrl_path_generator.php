<?php
/**
 * Path Generator Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class Ctrl_path_generator extends My_Controller
{
    //protected $_error_code = 10;
    public function __construct(){
        parent::__construct();
        $this->load->model('Junction_priority_m');
        $this->load->helper('string');
        $this->load->helper('xml');
        $this->load->library('graph');
        $this->load->library('bfs');
        ini_set('max_execution_time', 300);

    }   

    public function test(){
        $graph = [
            'gneJ18' => ['gneJ15'],
            'gneJ15' => ['gneJ18', 'gneJ8'],
            'gneJ8' => ['gneJ15', 'gneJ6', 'gneJ9', 'gneJ5']
        ];

        // // bfs($graph, 'A', 'D'); // true
        // // bfs($graph, 'A', 'G'); // false
        // print_r($this->bfs->bfs_path($graph, 'A', 'D'));

      // $graph = [
      //     'A' => ['B', 'C'],
      //     'B' => ['A', 'D'],
      //     'D' => ['B'],
      //     'C' => ['A',],
      // ];

      // $this->bfs->bfs($graph, 'A', 'D'); // true
      // $this->bfs->bfs($graph, 'A', 'G'); // false
      print_r($this->bfs->bfs_path($graph, 'gneJ18', 'gneJ5')); // ['A', 'B', 'D']
    }

    public function getPaths(){
      //getting all paths to an array
      $routesArray = array();
      if (file_exists('sumo/bus.trips.xml')) {
          $doc = new DOMDocument();
          $doc->load( 'sumo/bus.trips.xml' );
          $routes = $doc->getElementsByTagName( "routes" );
          foreach( $routes as $routes ) {
            $vehicles = $routes->getElementsByTagName( "vehicle" );
            foreach( $vehicles as $vehicle ){
              $routes = $vehicle->getElementsByTagName( "route" ); 
              $junctions = $routes->item(0)->getAttribute('junctions');
              $id = $vehicle->getAttribute('id');
              $a1 = array($id => $junctions);
              $routesArray = $routesArray + $a1;
            }
          }
      }
      var_dump($routesArray);
      // return $routesArray;
    }

    public function generatePaths($start, $end){
      // $start = 'gneJ18';
      // $end = 'gneJ6';
      $finalRes = [];
      $paths = $this->getPaths();
      $i = 0;
      foreach ($paths as $bus_id => $str_junctions) {
        $junctions = explode(" ", $str_junctions);
        if(in_array($start, $junctions)){
          $start_key = array_search($start, $junctions);
          $reponse = $this->findEnd($end, $junctions, $start_key, $edges, $edges);
          if($reponse){
            //on first iteration its working
            // $finalRes = $finalRes + array('bus_number' => $bus_id, 'junctions' => $reponse);
            //$array1 = array('bus_number' => $bus_id, 'junctions' => $reponse);
            $std2 = new stdClass;
            $std2->bus_number = $bus_id;
            $std2->junctions = $reponse;
            $tmp[] = $std2 ;
            $finalRes[] = $tmp;
            //array_merge($finalRes, 'route' => $array1);
            // var_dump(array('bus_number' => $bus_id, 'junctions' => $reponse));
            break;
          } else {
            $p_bus_id = $bus_id;
            $s_junctions = array_slice($junctions, $start_key);
            foreach ($s_junctions as $s_junction) {
              // 
              foreach ($paths as $bus_id => $str_junctions){
                $junctions = explode(" ", $str_junctions);
                if(in_array($s_junction ,$junctions) && in_array($end, $junctions)){
                  $tmp = [];
                  //start of getting index to ignore past
                  $temp_s = array_search($s_junction, $junctions);
                  $temp_e = array_search($end, $junctions);
                  if($temp_s > $temp_e){
                    continue;
                  }
                  //end of

                  //getting first bus junction details
                  $end_key = array_search($s_junction, $s_junctions);
                  $end_key++;
                  $p_junctions = array_slice($s_junctions, 0, $end_key);
                  // $finalRes = $finalRes + array('bus_number' => $p_bus_id, 'junctions' => $p_junctions);
                  // var_dump(array('bus_number' => $p_bus_id, 'junctions' => $p_junctions));
                  // echo '<br>';
                  // array_push($temRes, array('bus_number' => $p_bus_id, 'junctions' => $p_junctions));
                  $std2 = new stdClass;
                  $std2->bus_number = $p_bus_id;
                  $std2->junctions = $p_junctions;
                  $tmp[] = $std2 ;
                  
                  //getting second bus junction details
                  $start_key = array_search($s_junction, $junctions);
                  // var_dump($start_key);
                  // die();
                  $junctions = $this->findEnd($end, $junctions, $start_key);
                  // array_push($temRes, array('bus_number' => $bus_id, 'junctions' => $junctions));
                  $std2 = new stdClass;
                  $std2->bus_number = $bus_id;
                  $std2->junctions = $junctions;
                  $tmp[] = $std2 ;
                  $finalRes[] = $tmp;
                  // var_dump(array('bus_number' => $bus_id, 'junctions' => $junctions));
                  // echo '<br><br>';
                  //$finalRes = $finalRes + array('bus_number' => $bus_id, 'junctions' => $junctions);
                  // array_merge($finalRes, array('bus_number' => $bus_id, 'junctions' => $junctions));
                }
              }
              // array_merge($finalRes, 'route' => $temRes);
              
            }
          }
        }
        $i++;
      }
      var_dump($finalRes);
    }

    public function findEnd($end, $junctionsArray, $start_key, $edges){
      if(in_array($end, $junctionsArray)){
          $end_key = array_search ($end, $junctionsArray);
          $end_key = ($end_key - $start_key) + 1;
          $output = array_slice($junctionsArray, $start_key, $end_key);
          return $output;
      }
    }


    public function generatePath2()
    {


      if (file_exists('sumo/bus.trips.xml')) 
      {
          $final_result = array();
        	$doc = new DOMDocument();
             $doc->load( 'sumo/bus.trips.xml' );//xml file loading here

             $routes = $doc->getElementsByTagName( "routes" );
             $count=0;
           
             $start = 'gneJ15';
             $end = 'gneJ5';
             $final=array();

            foreach( $routes as $routes )
            {
               
               $vehicles = $routes->getElementsByTagName( "vehicle" );
              
               $result = [];
               $vehicleIndex = 0;
               foreach( $vehicles as $vehicle )
                {
                    if(!empty($result)){
                      $id=$vehicle->getAttribute('id');
                      var_dump($vehicles);
                      $routes = $vehicle->getElementsByTagName( "route" ); 
                      $junctions = $routes->item(0)->getAttribute('junctions');
                      $junctionsArray = explode(" ",$junctions);
                      $result = $this->findEnd($end, $junctionsArray, $start_key);
                      if($result){
                          $final = array('bus_number'=> $id,'junctions'=>$result);
                          array_push($final_result, $final);
                      }
                    } else{
                      //var_dump($vehicle);   
                    $routes = $vehicle->getElementsByTagName( "route" ); 
                    $junctions = $routes->item(0)->getAttribute('junctions');
                    $junctionsArray = explode(" ",$junctions);
                    //edges
                      $edges=$routes->item(0)->getAttribute('edges');
                     $edgesArray = explode(" ",$edges);
                   // $result[$vehicle->getAttribute('id')] = $junctionsArray ;

                    
                    if (in_array($start, $junctionsArray))
                     {
                                    $id=$vehicle->getAttribute('id');
                                   
                                    
                                    $start_key = array_search ($start, $junctionsArray);
                                    $arr_size = count($junctionsArray);
                                    $output = $this->findEnd($end, $junctionsArray, $start_key);
                                    $slicedArray = array_slice($junctionsArray, $start_key);
                                    if($output){
                                         $final = array('bus_number'=> $id,'junctions'=>$output);
                                         array_push($final_result, $final);

                                    } else{
                                      // foreach ($slicedArray as $slice) {

                                      //   for($i = $vehicleIndex; $i<sizeof($vehicles); $i++){
                                      //     $vehicle = $vehicles[$i];
                                      //     $id=$vehicle->getAttribute('id');
                                      //     var_dump($vehicles);
                                      //     var_dump($vehicleIndex);
                                        //   $routes = $vehicle->getElementsByTagName( "route" ); 
                                        //   $junctions = $routes->item(0)->getAttribute('junctions');
                                        //   $junctionsArray = explode(" ",$junctions);
                                        //   $result = $this->findEnd($end, $junctionsArray, $start_key);
                                        //   if($result){
                                        //      $final = array('bus_number'=> $id,'junctions'=>$result);
                                        //      array_push($final_result, $final);

                                        //   }
                                        // }
                                       }
                                    }
                    
                    $vehicleIndex++;
                  
                }
            // echo json_encode($id);
            }
            
          }
          var_dump($final_result);
      } else{
        $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'File not found');
        echo json_encode($error);
      }
    }   

    
     public function priority() {
        if (file_exists('sumo/map.net.xml')) 
	    {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here

            $nets  = $doc->getElementsByTagName("net");
             $edges  = $doc->getElementsByTagName("edge");

            $count=0;
            foreach( $nets as $net )
            {
                $junctions = $net->getElementsByTagName( "junction" );
                foreach( $junctions as $junction)
                {
                    $type=$junction->getAttribute('type');
                     $routes = $net->getElementsByTagName( "route" ); 
                   


                            
                      if($type=="priority" || $type=="dead_end")
                        {
                         // $node=$junction->getAttribute('id');
                           $node=$junction->getAttribute('id');
                         //echo "$node<br/>";
                          $nodes[]=$node;     

                                    
                         }
                    
                }
            } 
            $nodeCountArray = array();
            foreach($nodes as $node){
                $new_array = array($node => 0);
                $nodeCountArray = array_merge($nodeCountArray, $new_array);
            }
	    }
        {


      if (file_exists('sumo/bus.trips.xml')) 
      {
        	$doc2 = new DOMDocument();
             $doc2->load( 'sumo/bus.trips.xml' );//xml file loading here

             $routes = $doc2->getElementsByTagName( "routes" );
             $count=0;
           
           
            foreach( $routes as $routes )
            {
               
               $vehicles = $routes->getElementsByTagName( "vehicle" );
              
               $result = [];
               foreach( $vehicles as $vehicle )
                {
                    //var_dump($vehicle);   
                    $routes = $vehicle->getElementsByTagName( "route" ); 
                    $junctions = $routes->item(0)->getAttribute('junctions');
                    $junctionsArray = explode(" ",$junctions);
                    
                    foreach($junctionsArray as $junction)
                    {
                        if(array_key_exists($junction, $nodeCountArray)){
                            $nodeCountArray[$junction]++;
                        }
                    }
                }
            
            }
            $this->Junction_priority_m->saveData($nodeCountArray);
            echo 'Nodes extracted successfully';
      }
      else{
        $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'File not found');
        echo json_encode($error);
      }
    } 
        
        
    }       
}
