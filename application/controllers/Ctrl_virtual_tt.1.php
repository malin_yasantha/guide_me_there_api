<?php
/**
 * Virtual Time Table Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */
class Ctrl_virtual_tt extends My_Controller
{
    //protected $_error_code = 10;
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Test_m');
        $this->load->helper('string');
        $this->load->helper('xml');
    }   
                             
    public function test(){
        echo json_encode('testing');
    }

   public function getEdges($bus_id){
        if (file_exists('sumo/bus.trips.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/bus.trips.xml' );//xml file loading here
                                
            $vehicles = $doc->getElementsByTagName( "vehicle" );
            foreach( $vehicles as $vehicle )
            {
                $r_bus_id = $vehicle->getAttribute('id');
                if($r_bus_id == $bus_id){
                    $route = $vehicle->getElementsByTagName( "route" );
                    $edges = $route->item(0)->getAttribute('edges');
                    $edgesArray = explode(" ",$edges);
                    
                    return $edgesArray;

                     
                }
                
            }
            //bus not found
            return -1;
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

         public function getLengthOfEdge($edge_id){
        if (file_exists('sumo/map.net.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here
            
            $edges = $doc->getElementsByTagName( "edge" );
            foreach( $edges as $edge )
            {
                
                if($edge->getAttribute('id') == $edge_id){
                    $lane = $edge->getElementsByTagName( "lane" );
                    $length = $lane->item(0)->getAttribute('length');
                    return floatval($length);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }
               
    public function getAvgSpeedOfEdge($edge_id, $p_timestep){
        if (file_exists('sumo/output.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/output.xml' );//xml file loading here
            $result = array();
            $i = 0;
            $timesteps = $doc->getElementsByTagName( "timestep" );
            foreach( $timesteps as $timestep )
            {
                if($timestep->getAttribute('time') == $p_timestep){
                    $edges = $timestep->getElementsByTagName( "edge" );

                    foreach( $edges as $edge ){
                        if($edge->getAttribute('id') == $edge_id){
                            $lanes = $edge->getElementsByTagName( "lane" );
                            $avgSpeed = 0;
                            $i = 0;
                            foreach($lanes as $lane){
                                $vehicles = $lane->getElementsByTagName( "vehicle" );
                                foreach($vehicles as $vehicle){
                                    $avgSpeed += floatval($vehicle->getAttribute('speed'));
                                    $i++;
                                }
                            }
                            if($i > 1){
                                return ($avgSpeed/$i);
                            }
                            else{ 
                                return $this->getSpeedOfEdge($edge_id);
                            }
                        }
                    }
                    return $this->getSpeedOfEdge($edge_id);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

      public function getSpeedOfEdge($edge_id){
        if (file_exists('sumo/map.net.xml')) {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here
            
            $edges = $doc->getElementsByTagName( "edge" );
            foreach( $edges as $edge )
            {
                
                if($edge->getAttribute('id') == $edge_id){
                    $lane = $edge->getElementsByTagName( "lane" );
                    $speed = $lane->item(0)->getAttribute('speed');
                    return floatval($speed);
                }
            }
        }
        else{
            $error = array('status' => 'error', 'code' => $this->_error_code, 'msg' => 'Required Files not found');
            echo json_encode($error);
            die();
        }
    }

      public function calculateTravelTime($bus_id, $timestep, $start_edge, $end_node){
        $edges = $this->getEdges($bus_id);
        $start_key = array_search ($start_edge, $edges);
        $end_key = array_search ($end_edge, $edges);
        $outputs = array_slice($edges, $start_key, $end_key+1);
        foreach($outputs as $output){

            $edgeLength = $this->getLengthOfEdge($output);
            $avgSpeed = $this->getAvgSpeedOfEdge($output, $timestep);
            
           return $edgeLength/$avgSpeed;

        }
    }

    
    
    
    public function calculateBUSArrivalTime(){
        $bus_id = 'B1';
        $timestep = '3.00';
        $start_edge='gneE25';
        $end_edge='gneE6';
      


      $timestep += $this->calculateTravelTime($bus_id,$timestep , $start_edge, $end_edge ); 
      $array = array('bus_id' => $bus_id,'time' => $timestep , 'start'=>$start_edge, 'end'=>$end_edge);
      
       echo json_encode ($array);
  
        }

     public function getsedge ($start_node,$end_node)
     {
     

         if (file_exists('sumo/map.net.xml'))
          {
            $doc = new DOMDocument();
            $doc->load( 'sumo/map.net.xml' );//xml file loading here
            
           $finalarray =array('start_edge' => '', 'end_edge'=>'');
           $edges = $doc->getElementsByTagName( "edge" );
           foreach ($edges as $edge)
              {
                $priority = $edge->getAttribute('priority');
                                                           
                if (isset($priority) && $priority == 1)
                {       
                       if($edge->getAttribute('from') == $start_node)
                    {
                     $finalarray['start_edge'] = $edge->getAttribute('id');
                    }
                       if($edge->getAttribute('to') == $end_node)
                    {
                      $finalarray['end_edge'] = $edge->getAttribute('id'); 
                    }
                       
                }  

               }
            }

                      var_dump($finalarray);
     }

    
}
