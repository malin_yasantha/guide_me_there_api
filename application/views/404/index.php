<div class="modal-header">
    <h3 style="text-align: center;">Sorry! This page isn't available</h3>
    <p></p>
</div>
<div class="modal-body">
    <h6 style="text-align: center;">The Link you followed may be broken or the page may have removed.</h6>
</div>