<!DOCTYPE html>
<html lang="en">
<head>
  <title>Guide Me There - Admin Panel</title>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="<?= base_url('css/jquery.dataTables.min.css') ; ?>">
  <link rel="stylesheet" href="<?= base_url('css/bootstrap.min.css') ; ?>">
  <link rel="stylesheet" href="<?= base_url('css/jquery-ui.css') ; ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!--link rel="stylesheet" href="dist/css/font-awesome.min.css"-->
  <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet'>
  <link href='https://fonts.googleapis.com/css?family=Noto Sans' rel='stylesheet'>
  
  <script src="<?= base_url('js/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('js/jquery-ui.js'); ?>"></script>
  <script src="<?= base_url('js/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?= base_url('js/bootstrap.min.js'); ?> "></script>
  <script src="<?= base_url('js/common.js'); ?> "></script>
  
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body style="font-family: 'Noto Sans';">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" style="font-family: 'Ubuntu';">Guide Me There</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li <?php if($page_name == 'newsfeed'){ echo 'class="active"';}?>><a href="#" >News Feed</a></li>
        <li <?php if($page_name == 'chatbot'){ echo 'class="active"';}?>><a href="#">Chatbot Settings</a></li>
        <li <?php if($page_name == 'others'){ echo 'class="active"';}?>><a href="#">Other Settings</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">