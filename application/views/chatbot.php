
<script>

    $(document).ready(function(){
        //loadTable();
        $('#myTable').DataTable();

        $(document).on("click","#addProdBtn",function() {
            $('#addProdBtn').val('Please Wait');
            var csrf_pricecut = $('input[name=csrf_pricecut]').val();
            var product = $('#add_name').val();
            var price = $('#add_price').val();
            var is_active = 1;
            $.ajax({
                type: "POST",
                url: '<?=base_url("Ctrl_product/add_product")?>',
                dataType: 'json',
                data: {product: product, price: price, is_active: is_active, csrf_pricecut: csrf_pricecut},
                success: function(result){
                    console.log(result);
                    if(result.status == true){
                        $('#add_message').html('<div class="alert alert-success">' + result.message + '</div>');
                        location.reload();
                    }else{
                        $('#add_message').html('<div class="alert alert-danger">' + result.message + '</div>');
                    }
                    $('#add_message').fadeIn('slow');
                    setTimeout(function () {
                        $('#add_message').fadeOut('slow');
                    }, 1500);
                    $('#addProdBtn').val('Save');
                    //loadTable();
                    //$('#addModal').modal('hide');
                    $('#add_name').val('') ;
                    $('#add_price').val('');
                }
            });
        });

        $(document).on("click","#edtProdBtn",function() {
            $('#edtProdBtn').val('Please Wait');
            var csrf_pricecut = $('input[name=csrf_pricecut]').val();
            var product = $('#edit_name').val() ;
            var price = $('#edit_price').val() ;
            var edit_product_id = $('#edit_product_id').val() ;
            $.ajax({
                type: "POST",
                url: '<?=base_url("Ctrl_product/edit_product")?>',
                dataType: 'json',
                data: {product_id: edit_product_id, product: product, price: price, csrf_pricecut: csrf_pricecut},
                success: function(result){
                    console.log(result);
                    if(result.status == true){
                        $('#edit_message').html('<div class="alert alert-success">' + result.message + '</div>');
                        location.reload();
                    }else{
                        $('#edit_message').html('<div class="alert alert-danger">' + result.message + '</div>');
                    }
                    $('#edit_message').fadeIn('slow');
                    setTimeout(function () {
                        $('#edit_message').fadeOut('slow');
                    }, 1500);
                    $('#addProdBtn').val('Save');
                    //loadTable();
                    //$('#addModal').modal('hide');
                    $('#edit_name').val('') ;
                    $('#edit_price').val('');
                    $('#edit_product_id').val('');
                }
            });
        });

        $(document).on("click","#disDelBtn",function() {
            $('#edtProdBtn').val('Please Wait');
            var csrf_pricecut = $('input[name=csrf_pricecut]').val();
            var delID = $('#delID').val() ;
            $.ajax({
                type: "POST",
                url: '<?=base_url("Ctrl_product/remove_discount")?>',
                dataType: 'json',
                data: {product_id: delID, csrf_pricecut: csrf_pricecut},
                success: function(result){
                    console.log(result);
                    location.reload();
                }
            });
        });
    });
</script>

<div class="col-sm-2 sidenav">
</div>
<div class="col-sm-8 text-left">
    <h3><i class="fa fa-tags" aria-hidden="true"></i> Chatbot Settings <button title="add new" type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus" aria-hidden="true"></i></button> </h3>
    <hr>
    <table id="myTable" class="display">
        <thead>
        <tr>
            <th>Product Name</th>
            <th style="text-align:right">Price</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody id="dis_tbody">
        <!-- <?php
        foreach ($products as $product)
        {
        ?>
            <tr>
                <td><?=$product->product?></td>
                <td align="right"><?="Rs. ".number_format($product->price, 2)?></td>
                <td>
                    <button title="edit" type="button" class="btn btn-primary" data-toggle="modal" data-target="#edtModal" onclick="loadProduct('<?= $product->product_id; ?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    <button title="delete" type="button" class="btn btn-danger" data-toggle="modal" data-target="#dltModal" onclick="loadProduct('<?= $product->product_id; ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </td>
            </tr>
        <?php
        }
        ?> -->
        </tbody>
    </table>
</div>
<div class="col-sm-2 sidenav">

</div>


<!-- Delete Modal -->
<div class="modal fade" id="dltModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Product</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this product? </p>
                <input type="hidden" class="form-control" id="delID" placeholder="Enter title">
            </div>
            <div class="modal-footer">
                <button type="button" id="disDelBtn" class="btn btn-danger">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Disable Modal -->
<div class="modal fade" id="disModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Disable Product</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to disable this discount? </p>
                <input type="hidden" class="form-control" id="disID" placeholder="Enter title">
            </div>
            <div class="modal-footer">
                <button type="button" id="disDisBtn" class="btn btn-warning">Disable</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Update Modal -->
<div class="modal fade" id="edtModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modify Product</h4>
            </div>
            <div class="modal-body">
                <?php
                $arr_edit_form = array(
                    'class' => 'form-horizontal'
                );
                echo form_open('', $arr_edit_form);
                ?>
                    <div id="edit_message"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_name" placeholder="Enter name">
                            <input type="hidden" class="form-control" id="edit_product_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Price:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_price" placeholder="Enter price">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="edtProdBtn" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="addModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
            <div class="modal-body">
                <?php
                $arr_add_form = array(
                    'class' => 'form-horizontal'
                );
                echo form_open('', $arr_add_form);
                ?>
                    <div id="add_message"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_name" placeholder="Enter name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Price:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_price" placeholder="Enter price">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="addProdBtn" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>