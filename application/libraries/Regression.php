<?php
/**
 * Path Analyzer Controller
 *
 * @author     Malin Yasantha <malinyasantha@gmail.com>
 * @copyright  2017 - Guide Me There
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 * @link       http://pear.php.net/package/PackageName
 * @since      Class available since Release 1.0.0
 */

class Regression
{
    /**
     * @var int|double $a
     */
    private $a;
    /**
     * @var int|double $b
     */
    private $b;
    /**
     * @var int $n
     */
    private $n;
    /**
     * @var array $x
     */
    private $x = [];
    /**
     * @var array $y
     */
    private $y = [];
    /**
     * @var array $sqX
     */
    private $sqX = [];
    /**
     * @var array $sqY
     */
    private $sqY = [];
    /**
     * @var array $sqY
     */
    private $xy = [];
    


    /**
     * @return float|int
     */
    public function compluteY($x = NULL){
        if(!isset($x)){
            throw new \InvalidArgumentException('X value required');
        }
        return (($this->b * $x) + $this->a);
    }
    /**
     * @return float|int
     */
    private function compluteA(){
        $i = (array_sum($this->y) * array_sum($this->sqX)) - (array_sum($this->x) * array_sum($this->xy));
        $j = ($this->n * array_sum($this->sqX)) - pow(array_sum($this->x),2);
        $this->a = $i/$j;
    }
    /**
     * @return float|int
     */
    private function compluteB(){
        $i = ($this->n * array_sum($this->xy)) - (array_sum($this->x) * array_sum($this->y));
        $j = ($this->n * array_sum($this->sqX)) - pow(array_sum($this->x),2);
        $this->b = $i/$j;
    }
    /**
     * @return float|int
     */
    public function getA()
    {
        return $this->a;
    }
    /**
     * @return float|int
     */
    public function getB()
    {
        return $this->b;
    }
    public function getX()
    {
        return $this->x;
    }
    /**
     * @return array
     */
    public function getY()
    {
        return $this->y;
    }
    /**
     * @param array $xy
     *
     * @throws \InvalidArgumentException
     */
    public function setXY(array $xy)
    {
        if (0 === count($xy)) {
            throw new \InvalidArgumentException('XY can not be empty');
        }
        
        $x = [];
        $sqX = [];

        $y = [];
        $sqY = [];

        $n_xy = [];

        foreach ($xy as $key => $value) {
            $x[] = $key;
            $sqX[] = pow($key, 2);

            $y[] = $value;
            $sqY[] = pow($value, 2);

            $n_xy[] = $key * $value;
        }
        $this->n = count($xy);

        $this->xy = $n_xy;

        $this->setX($x, $sqX);
        $this->setY($y, $sqY);

        $this->compluteA();
        $this->compluteB();
    }
    /**
     * @param array $x
     *
     * @throws \InvalidArgumentException
     */
    private function setX(array $x, array $sqX)
    {
        if (0 === count($x) || 0 === count($sqX)) {
            throw new \InvalidArgumentException('X can not be empty');
        }
        if (count($x) !== count($sqX)) {
            throw new \InvalidArgumentException('Arguments not acceptable');
        }
        $this->x = $x;
        $this->sqX = $sqX;
    }
    /**
     * @param array $y
     *
     * @throws \InvalidArgumentException
     */
    private function setY(array $y, array $sqY)
    {
        if (0 === count($y) || 0 === count($sqY)) {
            throw new \InvalidArgumentException('Y can not be empty');
        }
        if (count($y) !== count($sqY)) {
            throw new \InvalidArgumentException('Arguments not acceptable');
        }
        $this->y = $y;
        $sqY = [];
        foreach($y as $i){
            $sqY[] = pow($i, 2);
        }
        $this->sqY = $sqY;
    }

}