<?php
/**
 * Contains class Regression.
 *
 * Class for computing multiple linear regression of the form
 * y=a+b1x1+b2x2+b3x3...
 *
 * PHP version 5.4
 *
 * LICENSE:
 * Copyright (c) 2011 Shankar Manamalkav <nshankar@ufl.edu>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    shankar<nshankar@ufl.edu>
 * @author    Michael Cummings<mgcummings@yahoo.com>
 * @copyright 2011 Shankar Manamalkav
 */
namespace mnshankar\LinearRegression;

class RegressionLib
{
    /**
     * @var array $x
     */
    private $x = [];
    /**
     * @var array $y
     */
    private $y = [];


    /**
     * @param array $x
     *
     * @throws \InvalidArgumentException
     */
    public function setX(array $x)
    {
        if (0 === count($x)) {
            throw new \InvalidArgumentException('X can not but empty');
        }
        $this->x = $x;
    }
    /**
     * @param array $y
     *
     * @throws \InvalidArgumentException
     */
    public function setY(array $y)
    {
        if (0 === count($y)) {
            throw new \InvalidArgumentException('Y can not but empty');
        }
        $this->y = $y;
    }
    /**
     * @param array $rawData
     * @param array $colsToExtract
     * @param int   $row
     *
     * @return array
     */
    private function getXArray(array $rawData, array $colsToExtract, $row)
    {
        $returnArray = [1];
        foreach ($colsToExtract as $key => $val) {
            $returnArray[] = $rawData[$row][$val];
        }
        return $returnArray;
    }
    /**
     * @param array $rawData
     * @param array $colsToExtract
     * @param int   $row
     *
     * @return array
     */
    private function getYArray(array $rawData, array $colsToExtract, $row)
    {
        $returnArray = [];
        foreach ($colsToExtract as $key => $val) {
            $returnArray[] = $rawData[$row][$val];
        }
        return $returnArray;
    }
}